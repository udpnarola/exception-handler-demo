package com.handle.exception.exception;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author puday
 */
@Component
public class GlobalExceptionHandler {

  public ResponseStatusException resourceNotFoundException(String msg) {
    return new ResponseStatusException(HttpStatus.NOT_FOUND, msg);
  }

  public void unAuthorizedException(String msg) {
    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, msg.concat(" You Are Unauthorized"));
  }

  public ResponseStatusException arithmeticException(String msg, Throwable throwable) {
    return new ResponseStatusException(HttpStatus.FORBIDDEN, msg, throwable);
  }

}
