package com.handle.exception.controller;

import com.handle.exception.exception.GlobalExceptionHandler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ExceptionTestController {

  private final GlobalExceptionHandler exceptionHandler;

  public ExceptionTestController(GlobalExceptionHandler exceptionHandler) {
    this.exceptionHandler = exceptionHandler;
  }

  @GetMapping("/file")
  public ResponseEntity<String> findFile(@RequestParam String fileName, @RequestParam Boolean exception){
    if(exception) {
      throw exceptionHandler.resourceNotFoundException(fileName);
    }
    return ResponseEntity.ok("Here Is Your File");
  }

  @GetMapping("/login")
  public ResponseEntity login(@RequestParam String userName, @RequestParam Boolean exception){
    if(exception) {
      exceptionHandler.unAuthorizedException(userName);
    }
    return ResponseEntity.ok("You Are Successfully Logged In");
  }

  @GetMapping("/divide")
  public ResponseEntity<Integer> divide(@RequestParam Integer one,@RequestParam Integer two){
    try{
      return ResponseEntity.ok(one / two);
    }catch (ArithmeticException e){
      throw exceptionHandler.arithmeticException("Your Number Must Be Invalid", e);
    }
  }
}
